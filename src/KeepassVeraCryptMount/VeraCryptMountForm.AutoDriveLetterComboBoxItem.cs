﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeePassVeraCryptMount
{
    public partial class VeraCryptMountForm
    {
        internal class AutoDriveLetterComboBoxItem
        {
            public override string ToString()
            {
                return "<Auto Select>";
            }
        }
    }
}
