﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassVeraCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
namespace KeePassVeraCryptMount
{
    using KeePass.Plugins;
    using KeePass.UI;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    public partial class VeraCryptOptionsForm : Form
    {
        private IPluginHost pluginHost;

        public VeraCryptOptionsForm(IPluginHost host)
        {
            this.pluginHost = host;

            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Icon = Icon.FromHandle(Resources.VeraCryptNormal.GetHicon());

            this.picBanner.BackgroundImage = BannerFactory.CreateBanner(
                this.picBanner.Width,
                this.picBanner.Height,
                BannerStyle.Default,
                Resources.B48_VeraCrypt,
                LanguageTexts.BannerPluginTitleText,
                LanguageTexts.BannerOptionsDescriptionText);

            this.txtVeraCryptExecutable.Text = this.pluginHost.GetVeraCryptExecutable();
            this.chkShowMenuItemAlways.Checked = this.pluginHost.GetVeraCryptMenuItemAlwaysVisible();
            this.numericAutoTypeTimeout.Value = this.pluginHost.GetVeraCryptAutoTypeWaitTimeout();

            this.OnVeraCryptPathTextChanged(this, e);

            GlobalWindowManager.AddWindow(this);

            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            GlobalWindowManager.RemoveWindow(this);

            base.OnClosing(e);
        }

        private void OnButtonOpenFileDialogClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtVeraCryptExecutable.Text))
            {
                try
                {
                    this.ofdVeraCryptExecutable.FileName = Path.GetFileName(this.txtVeraCryptExecutable.Text);
                    this.ofdVeraCryptExecutable.InitialDirectory = Path.GetDirectoryName(this.txtVeraCryptExecutable.Text);
                }
                catch (ArgumentException)
                {
                    Debug.WriteLine(string.Format("KeePassVeraCryptMount: Path contains invalid chars -> {0}", this.txtVeraCryptExecutable.Text));
                }
                catch (PathTooLongException)
                {
                    Debug.WriteLine(string.Format("KeePassVeraCryptMount: Path is to long -> {0}", this.txtVeraCryptExecutable.Text));
                }
            }

            if (this.ofdVeraCryptExecutable.ShowDialog(this) == DialogResult.OK)
            {
                this.txtVeraCryptExecutable.Text = this.ofdVeraCryptExecutable.FileName;
            }
        }

        private void OnOkButtonClicked(object sender, EventArgs e)
        {
            this.pluginHost.SetVeraCryptExecutable(this.txtVeraCryptExecutable.Text);
            this.pluginHost.SetVeraCryptMenuItemAlwaysVisible(this.chkShowMenuItemAlways.Checked);
            this.pluginHost.SetVeraCryptAutoTypeWaitTimeout((int)this.numericAutoTypeTimeout.Value);
        }

        private void OnVeraCryptPathTextChanged(object sender, EventArgs e)
        {
            var executableOk = VeraCryptInfo.ExecutableExists(this.txtVeraCryptExecutable.Text);

            this.txtVeraCryptExecutable.BackColor = executableOk
                ? SystemColors.Window 
                : Color.Coral;

            this.btnOk.Enabled = executableOk;
        }

        private void OnRegistryResolveButtonClicked(object sender, EventArgs e)
        {
            this.txtVeraCryptExecutable.Text = VeraCryptInfo.ResolveExecutableFromRegistry();
        }

        private void OnDonateLabelLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // start the prefered browser with paypal.
            var donationUri = @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N6FQKZTAX87ZQ";
            ProcessStartInfoExtensions.Execute(donationUri);
        }

        private void OnHelpLabelLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var helpUri = @"https://bitbucket.org/schalpat/keepassveracryptmount/wiki/Home";
            ProcessStartInfoExtensions.Execute(helpUri);
        }
    }
}
