﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassVeraCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassVeraCryptMount
{
    /* 
     * It seems like a mistake, but the plugin namespace must the same
     * than the plugin class name (with suffix Ext), so no dots allowed. 
     * The assembly file must also have the name of the namespace.
     */

    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    using KeePass.Plugins;

    public sealed class KeePassVeraCryptMountExt : Plugin
    {
        private IPluginHost pluginHost;
        private ToolStripMenuItem mnuEntryVeraCryptMount;
        private ToolStripMenuItem mnuToolsVeraCryptOptions;
        private MountMenuItemState mountMenuItemState;
        
        public KeePassVeraCryptMountExt()
        {
        }

        public override Image SmallIcon
        {
            get
            {
                return VeraCryptInfo.ExecutableExists(this.pluginHost.GetVeraCryptExecutable())
                    ? Resources.VeraCryptNormal
                    : Resources.VeraCryptError;
            }
        }

        public override string UpdateUrl
        {
            get
            {
                return @"https://bitbucket.org/ToVerify/keepassveracryptmount/downloads/version_manifest.txt";
            }
        }

        public override bool Initialize(IPluginHost host)
        {
            this.pluginHost = host;

            // insert the options menu item inside of the tools menu.
            var toolsMenuItem = this.pluginHost.MainWindow.ToolsMenu;

            this.mnuToolsVeraCryptOptions = new ToolStripMenuItem(LanguageTexts.TCOptionsMenuItemText + LanguageTexts.MenuItemOpenDialogSuffix);
            this.mnuToolsVeraCryptOptions.Image = Resources.VeraCryptNormal;
            this.mnuToolsVeraCryptOptions.Click += this.OnVeraCryptOptionsMenuItemClicked;

            toolsMenuItem.DropDownItems.Add(this.mnuToolsVeraCryptOptions);

            // insert the mount menu item inside of the entry menu.
            this.pluginHost.MainWindow.EntryContextMenu.KeyDown += this.OnEntryContextMenuKeyDown;
            this.pluginHost.MainWindow.EntryContextMenu.KeyUp += this.OnEntryContextMenuKeyUp;
            this.pluginHost.MainWindow.EntryContextMenu.Opened += this.OnEntryContextMenuOpened;
            this.mnuEntryVeraCryptMount = new ToolStripMenuItem();
            this.mnuEntryVeraCryptMount.ShortcutKeys = Keys.Control | Keys.T;
            this.mnuEntryVeraCryptMount.ShowShortcutKeys = true;
            this.mnuEntryVeraCryptMount.Click += this.OnVeraCryptMenuItemClicked;
            this.mnuEntryVeraCryptMount.Image = Resources.VeraCryptNormal;
            this.pluginHost.MainWindow.EntryContextMenu.Items.Insert(4, this.mnuEntryVeraCryptMount);
            this.mountMenuItemState = new MountMenuItemState(this.mnuEntryVeraCryptMount);
            this.mountMenuItemState.ChangeState(MountMenuItemStates.Invisible);
            this.mountMenuItemState.AlwaysVisible = this.pluginHost.GetVeraCryptMenuItemAlwaysVisible();

            this.SetVeraCryptMountMenuMenuAppearance();

            return true;
        }
        
        public override void Terminate()
        {
            this.pluginHost.MainWindow.ToolsMenu.DropDownItems.Remove(this.mnuToolsVeraCryptOptions);
            this.mnuToolsVeraCryptOptions.Click -= this.OnVeraCryptOptionsMenuItemClicked;
            this.mnuToolsVeraCryptOptions.Dispose();
            
            this.pluginHost.MainWindow.EntryContextMenu.KeyDown -= this.OnEntryContextMenuKeyDown;
            this.pluginHost.MainWindow.EntryContextMenu.KeyUp -= this.OnEntryContextMenuKeyUp;
            this.pluginHost.MainWindow.EntryContextMenu.Opened -= this.OnEntryContextMenuOpened;
            this.pluginHost.MainWindow.EntryContextMenu.Items.Remove(this.mnuEntryVeraCryptMount);
            this.mnuEntryVeraCryptMount.Click -= this.OnVeraCryptMenuItemClicked;
            this.mnuEntryVeraCryptMount.Dispose();
            base.Terminate();
        }

        private void OnEntryContextMenuKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
            {
                this.SetVeraCryptMountMenuItemAccessibility(true);
            }
        }
        
        private void OnEntryContextMenuKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
            {
                this.SetVeraCryptMountMenuItemAccessibility();
            }
        }

        private void OnEntryContextMenuOpened(object sender, EventArgs e)
        {
            this.SetVeraCryptMountMenuItemAccessibility();
        }

        private void OnVeraCryptMenuItemClicked(object sender, EventArgs e)
        {
            var entry = this.pluginHost.MainWindow.GetSelectedEntry(false);
            var database = this.pluginHost.Database;

            if (entry == null)
            {
                return;
            }

            if (this.mountMenuItemState.ShouldMountDialogShown)
            {
                var dialog = new VeraCryptMountForm()
                    .WithShowInTaskBar(false)
                    .WithStartPosition(FormStartPosition.CenterParent)
                    .ReadFrom(entry);

                var result = dialog.ShowDialog(this.pluginHost.MainWindow);

                if (result == DialogResult.Cancel)
                {
                    // exit here, save nothing, mount nothing
                    return;
                }

                // save settings
                if (dialog.ChangesApplied)
                {
                    dialog.WriteTo(entry);
                    entry.Touch(true);
                    this.pluginHost.Database.Modified = true;
                    this.pluginHost.MainWindow.UpdateUI(false, null, false, null, false, null, true);
                }
            }

            // do mount...
            var veracryptProcessInfo = new ProcessStartInfo(this.pluginHost.GetVeraCryptExecutable(),entry.ToVeraCryptArguments());

            var veracryptProcess = Process.Start(veracryptProcessInfo);
            if(veracryptProcess != null)
            {
                try
                {
                    if (veracryptProcess.WaitForInputIdle(this.pluginHost.GetVeraCryptAutoTypeWaitTimeout()))
                    {
                        entry.PerformPasswordAutotype(database);
                    }
                }
                catch (InvalidOperationException ex)
                {

                }
            }

        }

        private void OnVeraCryptOptionsMenuItemClicked(object sender, EventArgs e)
        {
            // open the plugin options dialog.
            using (var optionsDialog = new VeraCryptOptionsForm(this.pluginHost))
            {
                optionsDialog.WithShowInTaskBar(false)
                             .WithStartPosition(FormStartPosition.CenterParent)
                             .ShowDialog(this.pluginHost.MainWindow);
            }

            this.mountMenuItemState.AlwaysVisible = this.pluginHost.GetVeraCryptMenuItemAlwaysVisible();

            this.SetVeraCryptMountMenuMenuAppearance();
        }

        /// <summary>
        /// Changes the appearance and accessibilty of the mount menu item after option change.
        /// </summary>
        private void SetVeraCryptMountMenuMenuAppearance()
        {
            // in case of missing executeable, disable the menu item and load another overlay image.
            if (!VeraCryptInfo.ExecutableExists(this.pluginHost.GetVeraCryptExecutable()))
            {
                this.mnuEntryVeraCryptMount.Enabled = false;
                this.mnuEntryVeraCryptMount.Image = Resources.VeraCryptError;
            }
            else
            {
                this.mnuEntryVeraCryptMount.Enabled = true;
                this.mnuEntryVeraCryptMount.Image = Resources.VeraCryptNormal;
            }
        }

        private void SetVeraCryptMountMenuItemAccessibility(bool shiftHold = false)
        {
            var state = this.mountMenuItemState.GetState(
                this.pluginHost.MainWindow.GetSelectedEntriesCount(),
                this.pluginHost.MainWindow.GetSelectedEntry(false),
                shiftHold);

            this.mountMenuItemState.ChangeState(state);
        }
    }
}
